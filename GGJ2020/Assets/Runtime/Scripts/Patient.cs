﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patient : MonoBehaviour
{
    public int numWounds;

    private void Start()
    {
        FindObjectOfType<PuzzleManager>().NewPatientReset();
    }
}
