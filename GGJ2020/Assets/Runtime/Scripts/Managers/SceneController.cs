﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public void LoadStart()
    {
        SceneManager.LoadScene("01-PatientScreen");
    } 

    public void LoadHow()
    {
        //SceneManager.LoadScene("01-HowTo");
    }

    public void LoadLevel(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void LoadWin()
    {
        SceneManager.LoadScene("03-Win");
    }

    public void Reset()
    {
        SceneManager.LoadScene("BEGIN");
    }
}
