﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WoundManager : MonoBehaviour
{
    public Wound[] ListOfWounds;
    bool incomplete = true;

    void Start()
    {
        ListOfWounds = GetComponentsInChildren<Wound>();
    }

    public void CheckCompletion()
    {
        incomplete = false;
        for (int i = 0; i < ListOfWounds.Length; i++)
        {
            if (ListOfWounds[i].passConditionSatisfied == false)
            {
                incomplete = true;
            }
            //TODO LOAD THE PREVIOUS SCREEN
            //GetComponent<SceneController>().LoadStart();
        }

        if (!incomplete)
        {
            PuzzleManager.AddCompletedLevels();
        }
    }

    public void ResetAllWounds()
    {
        for (int i = 0; i < ListOfWounds.Length; i++)
        {
            ListOfWounds[i].ResetWound();
        }
    }
}
