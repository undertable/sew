using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreadManager : MonoBehaviour
{
    public float limit;     //length of the thread for this level
    public float minNodes;  //minimum amount of nodes the player needs to hit

    /*[SerializeField]
    GameObject thread;*/            //thread graphic that must be instantiated

    float currLength;
    int currIndex = 0;

    /*float bounds;

    List<int> tetherIndexes = new List<int>();
    Vector2 tether;*/

    bool drag;

    public LineRenderer line;
    List<Vector2> threadLink = new List<Vector2>();

    public void BeginThread(Vector2 pos)    //instantiate the first thread link at pos
    {
        /*tetherIndex = 0;
        tether = pos;
        threadLink.Add(Instantiate(thread, pos, Quaternion.identity));
        bounds = thread.transform.localScale.x;      //set bounds for thread link
        */

        Debug.Log("Begin");

        threadLink.Clear();
        threadLink.Add(pos);

        line = GetComponent<LineRenderer>();
        line.positionCount = threadLink.Count+1;
        line.SetPosition(line.positionCount - 1, GetWorldMousePos());
    }

    public Vector2 CalculateVectorDistance(Vector2 init, Vector2 final)
    {
        return new Vector2(init.x - final.x, init.y - final.y);
    }

    public void NodeHit(Vector2 pos)
    {
        Debug.Log(currLength);

        if (drag)
        {
            threadLink.Add(pos);
            line.positionCount++;

            line.SetPosition(line.positionCount - 1, GetWorldMousePos());

            //do raycast here
            RaycastHit2D hit = Physics2D.Linecast(threadLink[threadLink.Count - 2], threadLink[threadLink.Count - 1], LayerMask.GetMask("Wound"));

            if (hit)
            {
                hit.collider.gameObject.GetComponent<Wound>().UpdateCount();
                Debug.Log(hit.collider.name);
            }
        }
    }

    public void UpdateThread()
    {
        //line.SetPosition(1, GetWorldMousePos());

        //RaycastHit2D hit = Physics2D.LinecastAll(line.GetPosition(tetherIndexes),

        drag = true;

        for (int i = 0; i < line.positionCount-1; i++)
        {
            line.SetPosition(i, threadLink[i]);
        }

        currLength = CalculateTotalDistance(threadLink);

        //Debug.Log("Length of line: " + currLength);

        if (currLength > limit)
        {
            EndThread();
        }
        else
        {
            line.SetPosition(line.positionCount - 1, GetWorldMousePos());
        }
    }

    public float CalculateTotalDistance(List<Vector2> list)
    {
        float num = 0;

        for (int i = 0; i < list.Count-1; i++)
        {
            num += CalculateVectorDistance(threadLink[i + 1], threadLink[i]).magnitude;
        }

        num += CalculateVectorDistance(list[list.Count - 1], GetWorldMousePos()).magnitude;

        return num;
    }

    public void EndThread() //destroy thread and clear link
    {
        drag = false;
        line.positionCount = 0;
        currLength = 0;
        currIndex = 0;

        //Wound Manager Reset
        FindObjectOfType<WoundManager>().ResetAllWounds();

        Debug.Log("Thread released");
    }
    
    public Vector2 GetWorldMousePos()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}