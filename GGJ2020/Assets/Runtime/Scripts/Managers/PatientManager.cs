﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatientManager : MonoBehaviour
{
    public bool healed = false;
    public float secToDie = 1;
    public float speed = 10f;

    public GameObject txt;

    public GameObject currentPatient;
    public Vector2 activePos = Vector2.zero;
    public Vector2 spawnPos = new Vector2(12f, 0);
    public List<GameObject> patients = new List<GameObject>();

    public bool begin= false;
    public bool beginUsed = false;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        //instantiate first patient and store it in currPatient (should be manually put in list of patients in inspector)
        //currentPatient = Instantiate(currentPatient, spawnPos, Quaternion.identity);
        
    }

    private void Update()
    {
        if (begin)
        {
            currentPatient = Instantiate(patients[0], spawnPos, Quaternion.identity);
            begin = false;
            beginUsed = true;
        }

        if (currentPatient != null && (currentPatient.transform.position.x > activePos.x || healed))
        {
            currentPatient.transform.position += Vector3.left * speed * Time.deltaTime;
        }
    }

    public void Healed()
    {
        healed = true;
        Invoke("Kill", secToDie);
        currentPatient = patients[0];
        Instantiate(txt, currentPatient.transform.position, Quaternion.identity);
    }

    public void Kill()
    {
        //Destroy(currentPatient);
        currentPatient = null;
        NextPatient();
    }

    public void NextPatient()
    {
        if (patients.Count > 1)
        {
            healed = false;
            currentPatient = Instantiate(patients[1], spawnPos, Quaternion.identity);
        }
        else
        {
            FindObjectOfType<SceneController>().LoadWin();
        }


        patients.RemoveAt(0);
    }
}
