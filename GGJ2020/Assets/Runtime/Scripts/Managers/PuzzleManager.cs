﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Exists in the home screen

public class PuzzleManager : MonoBehaviour
{
    public static int numberCompleted;

    public static int numberOfWoundPuzzles;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void NewPatientReset()
    {
        numberCompleted = 0;
        numberOfWoundPuzzles = FindObjectOfType<Patient>().numWounds;
    }

    public static void AddCompletedLevels()
    {
        numberCompleted++;

        if (numberCompleted == numberOfWoundPuzzles)
        {
            Debug.Log("Level Complete");
            //TODO Instantiate the next patient
            FindObjectOfType<SceneController>().LoadStart();
            FindObjectOfType<PatientManager>().Healed();
        }
    }
}
