﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadStatics : MonoBehaviour
{

    public GameObject patient;
    public GameObject puzzle;

    // Start is called before the first frame update
    void Awake()
    {
        Instantiate(patient);
        Instantiate(puzzle);
    }

    private void Start()
    {
        SceneManager.LoadScene("00-Menu");
    }
}
