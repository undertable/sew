﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPatientManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (!FindObjectOfType<PatientManager>().beginUsed)
        {
            FindObjectOfType<PatientManager>().begin = true;
        }
    }
}
