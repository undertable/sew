using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    bool clicked = false;

    private Color defaultColor = new Color(0.0f, 0.0f, 0.0f);
    private Color satisfiedColor = new Color(0.9716981f, 0.7840739f, 0.6737718f);

    private void OnMouseDown()
    {
        //FindObjectOfType<ThreadManager>().BeginThread(transform.position);
        FindObjectOfType<ThreadManager>().BeginThread(FindObjectOfType<ThreadManager>().GetWorldMousePos());
        clicked = true;
        //Debug.Log(FindObjectOfType<ThreadManager>().CalculateDistance(FindObjectOfType<ThreadManager>().GetWorldMousePos(), transform.position));
    }

    private void OnMouseDrag()
    {
        FindObjectOfType<ThreadManager>().UpdateThread();
    }

    private void OnMouseUp()
    {
        FindObjectOfType<ThreadManager>().EndThread();
        clicked = false;
    }

    private void OnMouseOver()
    {
        if (!clicked)
        {
            FindObjectOfType<ThreadManager>().NodeHit(transform.position);
            gameObject.GetComponent<SpriteRenderer>().color = satisfiedColor;
        }
    }

    // Call this function when the player lifts their finger off the screen
    private void ResetColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = defaultColor;
    }
}