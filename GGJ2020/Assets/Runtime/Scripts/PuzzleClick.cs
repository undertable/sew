﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleClick : MonoBehaviour
{
    public string levelName;

    private void OnMouseDown()
    {
        GetComponent<SceneController>().LoadLevel(levelName);
    }
}
