using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wound : MonoBehaviour
{

    public bool passConditionSatisfied = false;

    [SerializeField]
    private int passValue;

    private int passCount;

    private Color defaultColor = new Color(1.0f, 1.0f, 1.0f);
    private Color satisfiedColor = new Color(0.4571022f, 0.9056604f, 0.5445849f);

    public void UpdateCount()
    {
        passCount++;

        CheckIfSatisfied();
    }

    public void ResetWound()
    {
        passCount = 0;
        passConditionSatisfied = false;
        UpdateColor();
    }

    public void CheckIfSatisfied()
    {
        if (passCount == passValue)
        {
            passConditionSatisfied = true;           
            UpdateColor();
            FindObjectOfType<WoundManager>().CheckCompletion();
        }
        else
        {
            passConditionSatisfied = false;
        }
    }

    public void UpdateColor()
    {
        if (passConditionSatisfied)
        {
            gameObject.GetComponent<SpriteRenderer>().color = satisfiedColor;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = defaultColor;
        }
    }
}