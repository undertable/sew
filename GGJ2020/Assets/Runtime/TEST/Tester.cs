﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour
{
    public bool test;

    // Update is called once per frame
    void Update()
    {
        if (test)
        {
            PuzzleManager.AddCompletedLevels();
            test = false;
        }
    }
}
